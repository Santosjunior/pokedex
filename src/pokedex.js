const pokedex = document.getElementById('pokedex');

const fetchPokemon = () => {
    //cria um arrey que recebe os dados da Pokéapi
    const promises = [];
    for (let i = 1; i <= 150; i++) {
        const url = `https://pokeapi.co/api/v2/pokemon/${i}`;
        promises.push(fetch(url).then((res) => res.json()));
    }
    Promise.all(promises).then((results) => {
        const pokemon = results.map((result) => ({
            name: result.name,
            image: result.sprites['front_default'],
            type: result.types.map((type) => type.type.name).join(', '),
            id: result.id,
            skill: result.moves.map((type) => type.move.name).join(', '),
            weight: result.weight,
            height: result.height
        }));
        displayPokemon(pokemon);
    });
};
// constante responsavel por montar o HTML da lista de pokemon na pagina
const displayPokemon = (pokemon) => {
    //lista os pokemons pelo nome, tipo e id
    console.log(pokemon);
    const pokemonHTMLString = pokemon
        .map(
            (pokeman) => `
        <li class="card">
            <img class="card-image" src="${pokeman.image}"/>
            <h2 class="card-title"> ${pokeman.name}</h2>
            <p class="card-subtitle"> Tipo: ${pokeman.type}  </p> 
            <p class="card-subtitle">${pokeman.id} </p>               
        </li>
    `
    // <p class="card-subtitle">Peso: ${pokeman.weight/10}KG Altura:${pokeman.height/10}M</p>
    // <p class="card-subtitle">Habilidades: ${pokeman.skill}</p>
        )
        .join('');
    pokedex.innerHTML = pokemonHTMLString;
};



fetchPokemon();
